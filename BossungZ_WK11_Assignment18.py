#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

''' Import the argument parsing library '''

import argparse
from prettytable import PrettyTable
from scapy.all import *
            
print("\nWelcome to the WiFi Sniffer Version 1.0\n")

print("BossungZ_WK11_Assignment18.py\nZack Bossung\nJanuary 2020\n")

''' Create a parser object '''
parser = argparse.ArgumentParser('Enter the WI-FI Adapter to Monitor')

''' Add possible arguments to the parser object '''
parser.add_argument('-v', "--verbose", help="dictates the level of output detail", action='store_true')
parser.add_argument('-w', '--wifi',  required=False, default='Wi-Fi', help="specify the network interface to monitor. Default: Wi-Fi")
parser.add_argument('-t', '--timeout', required=False, default=10, help="specify the amount of time to sniff packets. Default: 10")

''' Process the arguments '''
args = parser.parse_args()   

# Set the verbose status
if args.verbose:
    VERBOSE = True
else:
    VERBOSE = False

# Set the interface to monitor
NET_INTERFACE = args.wifi

## Set the timeout to monitor
timeout = int(args.timeout)

if VERBOSE:
    print("Attempting to Sniff Wifi Packets\n")
    print("++++Arguments used++++")
    print("Verbose: ", VERBOSE)
    print("Interface: ", NET_INTERFACE)
    print("Timeout: ", timeout, "\n")

# Configure the SCAPY interface
conf.iface = NET_INTERFACE

# Use Scapy to Sniff packets ... forever
# and report
pktCapture = sniff(timeout=timeout)

print("Finished capturing packets: ", pktCapture, "\nProcessing...\n")

tcpDict = {}
udpDict = {}

for pkt in pktCapture:
    if pkt.haslayer(TCP):
        tcpKey = str("TCP {} {} {} {}".format(pkt[IP].src, pkt[IP].dst, pkt[TCP].sport, pkt[TCP].dport))
        if tcpKey in tcpDict.keys():
            occur = tcpDict[(tcpKey)]
            occur += 1
            tcpDict[(tcpKey)] = occur
        else:
            tcpDict[(tcpKey)] = 1
    elif pkt.haslayer(UDP):
        udpKey = str("UDP {} {} {} {}".format(pkt[IP].src, pkt[IP].dst, pkt[UDP].sport, pkt[UDP].dport))
        if udpKey in udpDict.keys():
            occur = udpDict[(udpKey)]
            occur += 1
            udpDict[(udpKey)] = occur
        else:
            udpDict[(udpKey)] = 1
            
pTable = PrettyTable()
pTable.field_names = ["Protocol", "Source IP", "Destination IP", "Source Port", "Destination Port", "Occurences"]
        
for tcpEntry in tcpDict:
    tcpKeyEntry = str(tcpEntry).split(' ')
    pTable.add_row([tcpKeyEntry[0], tcpKeyEntry[1], tcpKeyEntry[2], tcpKeyEntry[3], tcpKeyEntry[4], tcpDict[tcpEntry]])
    
for udpEntry in udpDict:
    udpKeyEntry = str(udpEntry).split(' ')
    pTable.add_row([udpKeyEntry[0], udpKeyEntry[1], udpKeyEntry[2], udpKeyEntry[3], udpKeyEntry[4], udpDict[udpEntry]])    
    
print(pTable)